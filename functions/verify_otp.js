const admin = require("firebase-admin");

module.exports = (req, res) => {
    if (req.body.phone && req.body.otp) {
        const phone = String(req.body.phone).replace(/[^/g]/, "");
        const otp = parseInt(req.body.otp);

        admin.auth().getUser(phone)
            .then(() => {
                const ref = admin.database().ref('users/' + phone);
                ref.on('value', snapshot => {
                    const user = snapshot.val();
                    ref.off();
                    if (user.otp === otp && user.otpValidation) {
                        ref.update({ otpValidation: false });
                        admin.auth().createCustomToken(phone)
                            .then((token) => { res.send({ token: token }) });
                    } else {
                        return res.status(422).send({ error: 'otp is not valid' })
                    }
                });
            })
            .catch((err) => res.status(422).send({ error: err }));
    } else {
        res.status(422).send({ error: 'phone and otp must be provided' });
    }
};