const admin = require("firebase-admin");

module.exports = (req, res) => {
    //Verify the user phone provided
    if (req.body.phone) {
        //format the phone num to remove spaces dash etc
        const phone = String(req.body.phone).replace(/[^/g]/, "");

        // create new user account using that phone number
        admin.auth().createUser({ uid: phone })
            .then((user) => { res.send({ user: user, message: "user successfully created" }) })
            .catch(err => { res.status(422).send({ error: err }) });
    } else {
        return res.status(422).send({ error: "Bad request" });
    }
};