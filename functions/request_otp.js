const admin = require('firebase-admin');

const twilio = require('./confidentialData/twilio.js');

module.exports = (req, res) => {
    //Verify the user phone provided
    if (req.body.phone) {
        //format the phone num to remove spaces dash etc
        const phone = String(req.body.phone).replace(/[^/g]/, "");

        //check whether user exist or not
        admin.auth().getUser(phone)
            .then(userRecord => {
                const otp = Math.floor(Math.random() * 9876 + 1000);
                twilio.messages.create({
                    to: phone,
                    from: '+19032944367',
                    body: 'your one time password is' + otp,
                }, (err) => {
                    if (err) {
                        return res.status(422).send({ error: err });
                    }
                    admin.database().ref('users/' + phone)
                        .update({ otp: otp, otpValidation: true }, () => {
                            res.send({ success: true })
                        })
                });
            })
            .catch(err => res.status(422).send({ error: err }));
    } else {
        return res.status(422).send({ error: "You must provide phone number" });
    }
};