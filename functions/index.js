const functions = require('firebase-functions');
const admin = require("firebase-admin");

const serviceAccount = require('./confidentialData/service_account.json');
const createUser = require('./create_user');
const requestOTP = require('./request_otp');
const verifyOTP = require('./verify_otp');

//Twilio phone no: +19032944367

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://one-time-password-29fb1.firebaseio.com"
});

exports.createUser = functions.https.onRequest(createUser);
exports.requestOTP = functions.https.onRequest(requestOTP);
exports.verifyOTP = functions.https.onRequest(verifyOTP);